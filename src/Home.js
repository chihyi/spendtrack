import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import AddData from './AddData'

const useStyles = makeStyles((theme) => ({

}));

export default function Home() {
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };
    return (
        <div>
            <Button variant="outlined" color="primary" onClick={handleClickOpen}>
                Add spend recod
            </Button>
            <AddData open={open} setOpen={setOpen}/>
        </div>
    )
}
