import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import ImageUpload from './ImageUpload'
import _ from 'lodash'


const useStyles = makeStyles((theme) => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  container: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column"
  },
  textField: {
    margin: "10px",
    width: "95%"
  },
  img: {
    position: "absolute",
    bottom: 0
  }
}));



export default function FullScreenDialog(props) {
  const classes = useStyles();
  const [record, setRecord] = React.useState({});

  const handleClose = () => {
    props.setOpen(false);
    let res = {
      "items": window.Sprs
    }
    console.log(JSON.stringify(res))
  };

  const handleChange = (key, value) => {
    setRecord({
      ...record,
      [key]: value
    })
  }

  const handleSave = () => {

    if (window.Sprs) {
      window.Sprs.push(record)

    } else {
      window.Sprs = []
      window.Sprs.push(record)
    }
    setRecord({})
  }

  const renderSubtype = () => {

    switch (_.get(record, "type", "")) {
      case "food":
        return [
          <MenuItem value={"breakfast"}>breakfast</MenuItem>,
          <MenuItem value={"lunch"}>lunch</MenuItem>,
          <MenuItem value={"dinner"}>dinner</MenuItem>,
          <MenuItem value={"snaks"}>snaks</MenuItem>,
          <MenuItem value={"ingredients"}>ingredients</MenuItem>
        ]

      case "transportation":
        return [
          <MenuItem value={"parking fee"}>parking fee</MenuItem>,
          <MenuItem value={"gas"}>gas</MenuItem>,
          <MenuItem value={"public"}> public transportation</MenuItem>,
        ]


      case "housing":
        return [
          <MenuItem value={"rent"}>rent</MenuItem>,
          <MenuItem value={"common-charges"}>common charges</MenuItem>,
          <MenuItem value={"utilities-bill"}>utilities bill</MenuItem>
        ]

      // eslint-disable-next-line no-duplicate-case
      case "clothing":
        return [
          <MenuItem value={"cloths"}>cloths</MenuItem>,
          <MenuItem value={"shose"}>shose</MenuItem>
        ]

      case "daily-necessities":
        return [
          <MenuItem value={"grocery"}>grocery</MenuItem>,
          <MenuItem value={"toiletries"}>toiletries</MenuItem>,
          <MenuItem value={"pharmacy"}>pharmacy</MenuItem>
        ]
      case "entertainment":
        return [
          <MenuItem value={"gym"}>gym</MenuItem>,
          <MenuItem value={"ticket"}>ticket</MenuItem>,
          <MenuItem value={"hotel"}>hotel</MenuItem>
        ]
      default:
        break;
    }
  }

  return (
    <div>
      <Dialog fullScreen open={props.open} onClose={handleClose} TransitionComponent={props.transition}>


        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              Consumption recod ({_.get(window, "Sprs.length", 0)})
            </Typography>
            <Button autoFocus color="inherit" onClick={handleSave}>
              save
            </Button>
          </Toolbar>
        </AppBar>
        <form className={classes.container} noValidate>
          <TextField
            id="date"
            label="Date"
            type="date"
            defaultValue={new Date().toISOString().split("T")[0]}
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
            value={_.get(record, "time", "")}

            onChange={(e) => { handleChange("time", e.target.value) }}
          />
          <FormControl className={classes.textField}>
            <InputLabel id="demo-simple-select-label">Type</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={_.get(record, "type", "")}

              onClick={(v) => { handleChange("type", v.target.value) }}
            >
              <MenuItem value={"food"}>food</MenuItem>
              <MenuItem value={"housing"}>housing</MenuItem>
              <MenuItem value={"transportation"}>transportation</MenuItem>
              <MenuItem value={"clothing"}>clothing</MenuItem>
              <MenuItem value={"entertainment"}>entertainment</MenuItem>
              <MenuItem value={"daily-necessities"}>daily necessities</MenuItem>

            </Select>
          </FormControl>
          <FormControl className={classes.textField}>
            <InputLabel id="demo-simple-select-label">sub type</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={_.get(record, "subtype", "")}
              onChange={(v) => { handleChange("subtype", v.target.value) }}
            >
              {renderSubtype()}

            </Select>
          </FormControl>
          <FormControl className={classes.textField}>
            <InputLabel id="demo-simple-select-label">Payment</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={_.get(record, "payment", "")}
              onChange={(e) => { handleChange("payment", e.target.value) }}
            >
              <MenuItem value={"cash"}>Cash</MenuItem>
              <MenuItem value={"credit-card"}>Credit card</MenuItem>
              <MenuItem value={"remittance"}>remittance</MenuItem>
            </Select>
          </FormControl>

          <FormControl className={classes.textField}>
            <InputLabel htmlFor="standard-adornment-amount">Cost</InputLabel>
            <Input
              id="standard-adornment-amount"
              value={_.get(record, "cost", "")}
              type={"number"}
              onChange={(e) => { handleChange("cost", e.target.value) }}
              startAdornment={<InputAdornment position="start">$</InputAdornment>}
            />

          </FormControl>
          <FormControl className={classes.textField}>
            <InputLabel htmlFor="standard-adornment-amount">Split</InputLabel>
            <Input
              id="standard-adornment-amount"
              value={_.get(record, "split", "")}
              type={"number"}
              onChange={(e) => { handleChange("split", e.target.value) }}
              startAdornment={<InputAdornment position="start">Num:</InputAdornment>}
            />
          </FormControl>
          <FormControl className={classes.textField}>
            <InputLabel htmlFor="standard-adornment-amount">Location</InputLabel>
            <Input
              id="standard-adornment-amount"
              value={_.get(record, "location", "")}
              onChange={(e) => { handleChange("location", e.target.value) }}
              startAdornment={<InputAdornment position="start">At:</InputAdornment>}
            />
          </FormControl>
          <FormControl className={classes.textField}>
            <InputLabel htmlFor="standard-adornment-amount">Description</InputLabel>
            <Input
              id="standard-adornment-amount"
              value={_.get(record, "desc", "")}
              onChange={(e) => { handleChange("desc", e.target.value) }}
            />
          </FormControl>
          <div className={classes.img}>
            <ImageUpload />
          </div>
        </form>
      </Dialog>
    </div>
  );
}
